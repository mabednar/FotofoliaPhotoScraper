#!/usr/bin/env python
# coding=utf-8

# author: Marek Bednarek
# date: 26.12.2016


import argparse
import requests
import logging
# import PIL
import re
from lxml import html

import io
import urllib.request as urllib
from PIL import Image

logging.basicConfig(format='%(asctime)s %(name)s %(levelname)s %(message)s')
console = logging.getLogger('scraper')


class FotofoliaPhotoScraper():

    def __init__(self, verbose='False'):

        if verbose == True:
            console.setLevel(logging.INFO)
        elif verbose:
            console.setLevel(logging.verbose)   # TODO add list of available log levels
        else:
            console.propagate = False

    def scrape(self, url, file=None, zoom=3):
        self.url = url
        self.zoom = zoom
        src_url, dimensions, zoom = self._get_image_info()

        if file == None:
            self.file = self._create_file_name(src_url)
        self._save_image(src_url)

    def get_image_name(self):
        '''fetch image name, remove all non word characters'''
        tree = self._read_url(self.url)
        raw_name = tree.xpath('//*[@class="h-strong content-title truncate"]/text()')[0]
        return re.sub(r'\W', '_', raw_name)

    def _get_image_id(self):
        return self.url.rsplit('/').pop(-1)

    def _get_image_info(self):
        tree = self._read_url(self.url)
        d = tree.xpath('//*[@class="ftl-zoom content-detail-thumbnail"]')[0][0].attrib
        image_src = self._get_image_src(d)
        dim = self._get_image_dimensions(d)
        zoom = d['data-zoom_ratio']
        return image_src, dim, zoom

    def _get_image_src(self, info_dict):
        image_src = info_dict['src']
        console.info('Image source: {}'.format(image_src))
        return image_src

    def _get_image_dimensions(self, info_dict):
        dim = (info_dict['height'], info_dict['width'])
        console.info('Image height: {}, width: {}'.format(dim[0], dim[1]))
        return dim

    def _get_image_extension(self, src_url):
        return src_url.rsplit('.').pop(-1)

    def _prepere_zoom_image_url(self):
        pass
        foto_url.replace('https:', 'https%3A')

    def _create_file_name(self, src_url):
        img_name = self.get_image_name()
        img_id = self._get_image_id()
        img_ext = self._get_image_extension(src_url)
        filename = ''.join([img_name, img_id, '.', img_ext])
        console.info('Image filename created: {}'.format(filename))
        return filename

    def _save_image(self, src_url):
        file = urllib.urlopen(src_url)
        im = io.BytesIO(file.read())
        img = Image.open(im)
        (mx, my) = img.size
        print(mx, my)
        del img
        del im

    def _read_url(self, url):
        page = requests.get(url, timeout=2)
        tree = html.fromstring(page.content)
        return tree




# def cmd_launch():
#     parser = argparse.ArgumentParser()
#     parser.add_argument('image_url', help='fotofolia url of image ie: https://pl.fotolia.com/id/129322446')
#     parser.add_argument('-f', '--file', help='file name. If not given it will be scrape from page')
#     parser.add_argument('-z', '--zoom', help='image zoom (integer)', type=integer)
#     parser.add_argument('-v', '--verbose', help='increase output verbosity', action='store_true')
#     args = parser.parse_args()
#     print(args)
#     FotofoliaPhotoScraper(verbose=args.verbose).scrape(url=args.image_url, file=args.file, zoom=args.zoom)

# if __name__ == '__main__':
#     cmd_launch()



FotofoliaPhotoScraper(verbose=True).scrape(url='https://pl.fotolia.com/id/129322446')
