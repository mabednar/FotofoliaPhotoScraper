#!/usr/bin/env python
# -*- coding: utf-8 -*-

import math
import sys
import urllib
import Image
import cStringIO
import time

def get_iimage(url):
    print("pobieram "+url)
    while True:
        try:
            file = urllib.urlopen(url)
            im = cStringIO.StringIO(file.read())
            img = Image.open(im)
        break
        except IOError:
            print "Problem z pobraniem obrazka, czekam 3 minuty"
            time.sleep(180)
    del im
    return img

def sr_kwadrat((r1,g1,b1), (r2,g2,b2)):
    return (r1-r2)**2+(g1-g2)**2+(b1-b2)**2

if ( len(sys.argv) < 3):
    print ("""Użycie:
    pobieracz_foto nazwa_pliku adres_strony [zoom [napisy]]

    format pliku w jakim zostanie zapisane zdjęcie zależy od końcówki nazwy (rozszerzenia) zalecane .png albo .jpeg
    zoom określa ile razy większy będzie obraz wynikowy od miniaturki zamieszczonej na stronie
    jeśli nie zostanie podany, domyślny zoom = 3
    podanie parametru napisy powoduje, że obrazki będą pobrane razem z napisami (jest to szybsze ale brzydko wygląda)
    """)
    exit(0)

nazwa_pliku=sys.argv[1]

base_url=sys.argv[2]

if ( len(sys.argv) < 4):
    zoom=3
else:
    zoom=int(sys.argv[3])

html_file = urllib.urlopen(base_url)
for linia in html_file.readlines():
    if linia.find('<meta name="twitter:image" content') <> -1 :
        tt=linia.split('"')
        m_url=tt[3]

html_file.close()
print("Adres miniaturki: "+m_url)

file = urllib.urlopen(m_url)
im = cStringIO.StringIO(file.read())
img = Image.open(im)
(mx, my) = img.size
del img
del im

print("Wymiary miniaturki: "+str(mx)+"x"+str(my))
bx=mx*zoom
by=my*zoom
print("Przewidywane wymiary pliku wynikowego: "+str(bx)+"x"+str(by))

mapa = Image.new("RGB", (bx,by))

url_s="http://zoom.fotolia.com/Content/Zoom/"

yc=0
for y in xrange(zoom):
    xc=0
    for x in xrange(zoom):
        url=url_s+str(zoom)+"/"+str(mx*(x+0.5)/float(zoom))+"/"+str(my*(y+0.5)/float(zoom))+"/?path="+m_url
        img=get_iimage(url)
        if (x==0) and (y==0):
            (mx, my) = img.size
        if ((y>0) and (x==0)):
            #korekcja y
            minbl=15000
            besty=yc
for yi in range (yc-5*zoom,yc):
    	blad=0
    	for i in xrange (100):
    	    blad+=sr_kwadrat(img.getpixel((i, 0)),mapa.getpixel((x*mx+i,y*my+yi)) )
    	#print "przesuniecie: "+str(yi)+" różnica: "+str(blad)
    	if (blad<minbl):
    	    besty=yi
    	    minbl=blad
            if ((minbl<15000) and (besty<yc-1)):
	           yc=besty
      #print ("korekcja Y: "+str(yc)+"  suma_różnic:"+str(minbl))

    if (x>0):
        #korekcja x
        minbl=300*my
        bestx=xc
        for xi in range (xc-5*zoom-1,xc):
	blad=0
	for i in xrange (my):
	    blad+=sr_kwadrat(img.getpixel((0, i)),mapa.getpixel((x*mx+xi,y*my+yc+i)) )
	#print "przesuniecie: "+str(xi)+" różnica: "+str(blad)
	if (blad<minbl):
	    bestx=xi
	    minbl=blad
        if ((minbl<300*my) and (bestx<xc-1)):
            xc=bestx
      #print ("korekcja X: "+str(xc)+"  suma_różnic:"+str(minbl))

    mapa.paste(img, (x*mx+xc,y*my+yc,(x+1)*mx+xc,(y+1)*my+yc))
    del img
mainxc=xc
if (len(sys.argv)<5 and (mx==500)):
    xc=0
    for x in xrange(1,zoom):
      url=url_s+str(zoom)+"/"+str(mx*x/float(zoom))+"/"+str(my*(y+0.5)/float(zoom))+"/?path="+m_url
      img=get_iimage(url)

      minbl=300*my
      bestx=xc
      for xi in range (xc-5*zoom-1,xc+zoom*3):
	blad=0
	for i in xrange (my):
	  blad+=sr_kwadrat(img.getpixel((100, i)),mapa.getpixel((int((x-0.5)*mx)+xi+100,y*my+yc+i)) )
	#print "przesuniecie: "+str(xi)+" różnica: "+str(blad)
	if (blad<minbl):
	  bestx=xi
	  minbl=blad
      if ((minbl<300*my) and (bestx<xc+zoom*3-1)):
	xc=bestx
      #print ("korekcja AX: "+str(xc)+"  suma_różnic:"+str(minbl))

      imgA=img.crop((0,0,125,my))
      imgB=img.crop((375,0,500,my))
      mapa.paste(imgA, (int((x-0.5)*mx)+xc,y*my+yc,int((x-0.5)*mx)+125+xc,(y+1)*my+yc))

      minbl=300*my
      bestx=xc
      for xi in range (xc-5*zoom-1,xc+zoom*3):
	blad=0
	for i in xrange (my):
	  blad+=sr_kwadrat(imgB.getpixel((0, i)),mapa.getpixel((int((x-0.5)*mx)+xi+300,y*my+yc+i)) )
	#print "przesuniecie: "+str(xi)+" różnica: "+str(blad)
	if (blad<minbl):
	  bestx=xi
	  minbl=blad
      if ((minbl<300*my) and (bestx<xc+zoom*3-1)):
	xc=bestx
      #print ("korekcja BX: "+str(xc)+"  suma_różnic:"+str(minbl))

      mapa.paste(imgB, (int((x-0.5)*mx)+375+xc,y*my+yc,int((x-0.5)*mx)+500+xc,(y+1)*my+yc))
      del imgA
      del imgB
      del img
    url=url_s+str(zoom)+"/"+str(mx*(0.75)/float(zoom))+"/"+str(my*(y+0.5)/float(zoom))+"/?path="+m_url
    img=get_iimage(url)

    xc=0
    minbl=300*my
    bestx=xc
    for xi in range (xc-5*zoom-1,xc+zoom*3):
	blad=0
	for i in xrange (my):
	  blad+=sr_kwadrat(img.getpixel((300, i)),mapa.getpixel(   (int(0.25*mx)+xi+300   ,   y*my+yc+i)   ) )
	#print "przesuniecie: "+str(xi)+" różnica: "+str(blad)
	if (blad<minbl):
	  bestx=xi
	  minbl=blad
    if ((minbl<300*my) and (bestx<xc+zoom*3-1)):
	xc=bestx
    #print ("korekcja X: "+str(xc)+"  suma_różnic:"+str(minbl))

    imgA=img.crop((0,0,125,my))
    mapa.paste(imgA, (int((0.25)*mx)+xc,y*my+yc,int(0.25*mx)+125+xc,(y+1)*my+yc )  )
    del imgA
    del img
    x=zoom-1
    url=url_s+str(zoom)+"/"+str(mx*(x+0.22)/float(zoom))+"/"+str(my*(y+0.5)/float(zoom))+"/?path="+m_url
    img=get_iimage(url)

    xc=mainxc
    minbl=300*my
    bestx=xc
    for xi in range (xc-7*zoom-1,xc+zoom*5):
	blad=0
	for i in xrange (my):
	  blad+=sr_kwadrat(img.getpixel((100, i)),mapa.getpixel(  ( int((x-0.25)*mx)+xi+100,  y*my+yc+i )  ))
	#print "przesuniecie: "+str(xi)+" różnica: "+str(blad)
	if (blad<minbl):
	  bestx=xi
	  minbl=blad
    if ((minbl<300*my) and (bestx<xc+zoom*5-1)):
	xc=bestx
    #print ("korekcja X: "+str(xc)+"  suma_różnic:"+str(minbl))

    imgB=img.crop((375,0,500,my))
    mapa.paste(imgB, (int((x-0.25)*mx)+375+xc,y*my+yc,int((x-0.25)*mx)+500+xc,(y+1)*my+yc))
    del imgB
    del img

print("ostateczne wymiary "+str(bx+mainxc)+"x"+str(by+yc) )
mapa.crop((0,0,bx+mainxc,by+yc)).save(nazwa_pliku)
