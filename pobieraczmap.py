#!/usr/bin/env python
# -*- coding: utf-8 -*-

import math
import sys
import urllib
import Image
import cStringIO

def deg2num(lat_deg, lon_deg, zoom):
  lat_rad = math.radians(lat_deg)
  n = 2.0 ** zoom
  xtile = int((lon_deg + 180.0) / 360.0 * n)
  ytile = int((1.0 - math.log(math.tan(lat_rad) + (1 / math.cos(lat_rad))) / math.pi) / 2.0 * n)
  return (xtile, ytile)

if ( len(sys.argv) < 4):
  print ("""Użycie: 
  pobieraczmap nazwa_pliku szerokość_geograficzna długość_geograficzna [zoom [szerokość_mapy wysokość_mapy [typ]]]
  
  formt pliku w jakim zostanie zapisana mapa zależy od końcówki nazwy (rozszerzenia) zalecane .png albo .jpeg
  długość i szerokość gograficzną nalezy podać w stopniach jako liczby rzeczywiste np 51.072 21.01
  najlepiej przekopiować te liczby z linka do mapy
  domyślny zoom = 16, maksymalny zoom = 20 jednak nie na wszytkich obszarach jest on dostępny
  wysokość i szerokość mapy podajemy w kafelkach, rozmiar jednego kafelka to 256×256 pixele 
  domyślny rozmiar mapy to 4×4 co odpowiada plikowi o wymiarach 1024×1024px
  możliwe typy: satelita, mapa, hybryda
  domyślny typ: satelita
  """)
  exit(0)
  
nazwa_pliku=sys.argv[1]

if ( len(sys.argv) < 5):
  zoom=16
else:
  zoom=int(sys.argv[4])

if ( len(sys.argv) < 7):
  szer=4
  wys=4
else:
  szer=int(sys.argv[5])
  wys=int(sys.argv[6])

(xs,ys)= deg2num(float(sys.argv[2]),float(sys.argv[3]),zoom)

mapa = Image.new("RGB", (szer*256,wys*256))

xs-=szer/2
ys-=wys/2

if ( (len(sys.argv) < 8) or sys.argv[7]=="satelita"):
  url_s="https://khms0.google.com/kh/v=166&src=app&x="
elif (sys.argv[7]=="mapa") :
  url_s="https://mts0.google.com/vt/lyrs=m@248000000&hl=pl&src=app&x="
elif (sys.argv[7]=="hybryda"):
  url_s="http://mt1.google.com/vt/lyrs=y&x="
else:
  print("Nieznany typ "+sys.argv[7])
  exit(0)

for x in xrange(szer):
  for y in xrange(wys):
    url=url_s+str(xs+x)+"&y="+str(ys+y)+"&z="+str(zoom)+"&s="
    file = urllib.urlopen(url)
    im = cStringIO.StringIO(file.read())
    img = Image.open(im)
    mapa.paste(img, (x*256,y*256,(x+1)*256 ,(y+1)*256))
    del img
    del im
mapa.save(nazwa_pliku)
